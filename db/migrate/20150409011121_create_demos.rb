class CreateDemos < ActiveRecord::Migration
  def change
    create_table :demos do |t|
      t.string :name
      t.string :description
      t.integer :number

      t.timestamps null: false
    end
  end
end
