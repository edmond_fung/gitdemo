json.array!(@demos) do |demo|
  json.extract! demo, :id, :name, :description, :number
  json.url demo_url(demo, format: :json)
end
